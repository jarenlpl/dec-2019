// main js file


// update copyright year

const copyrightYear = document.querySelector('#copyrightYear');

function copyrightUpdate() {
	let dt = new Date().getFullYear();
	copyrightYear.innerHTML = dt;
}
copyrightUpdate();


// menu toggle nav

const headerCon = document.querySelector('.header-con');
const btnCon = document.querySelector('.header__menu .btn');
const btnIcon = document.querySelectorAll('.header__menu .btn i');
const navCon = document.querySelector('.header__nav');

function toggleBtnVisible() {

	let headerConHeight = headerCon.clientHeight;

	if (navCon.offsetTop < 0) {
		navCon.style.top = headerConHeight + 'px';
		navCon.style.opacity = 1;
	} else {
		navCon.style.top = '-100%';
		navCon.style.opacity = 0;
	}

	for(let i = 0; i < btnIcon.length; i++) {
		btnIcon[i].classList.toggle('active');
	}

}

btnCon.addEventListener('click', toggleBtnVisible);
btnCon.addEventListener('keyPress', toggleBtnVisible);
