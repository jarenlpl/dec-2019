# Testing Gitlab Pages

Simple test site to run from local dev environment to a static site.

View the live site here: [jaren.lpl.gitlab.io/dec-2019/](https://jaren.lpl.gitlab.io/dec-2019/)
